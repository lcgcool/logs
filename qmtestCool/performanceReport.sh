#! /bin/bash

###echo "performanceReport.sh - You must comment out this line!"; exit 1

# Check arguments - define the platforms to analyze
thePlatformsSl5="i686-slc5-gcc43-dbg\
                 i686-slc5-gcc43-opt\
                 i686-slc5-icc11-dbg\
                 i686-slc5-icc11-opt\
                 x86_64-slc5-gcc43-dbg\
                 x86_64-slc5-gcc43-opt\
                 x86_64-slc5-icc11-dbg\
                 x86_64-slc5-icc11-opt"
thePlatformsOsx="i386-mac106-gcc42-dbg\
                 i386-mac106-gcc42-opt\
                 x86_64-mac106-gcc42-dbg\
                 x86_64-mac106-gcc42-opt"
thePlatformsWin="i686-winxp-vc9-dbg"
if [ "$2" != "" ] || [ "$1" == "" ]; then
  echo "Usage: `basename ${0}` all|sl5|osx|win|<CMTCONFIGs>"
  exit 1
elif [ "$1" == "all" ]; then
  thePlatforms=`echo $thePlatformsSl5 $thePlatformsOsx $thePlatformsWin`
elif [ "$1" == "sl5" ]; then
  thePlatforms=`echo $thePlatformsSl5`
elif [ "$1" == "osx" ]; then
  thePlatforms=`echo $thePlatformsOsx`
elif [ "$1" == "win" ]; then
  thePlatforms=`echo $thePlatformsWin`
else
  thePlatforms="$*"
fi

# Remove the arguments - otherwise they confuse CMT setup.sh
shift $#

# Locate the qmtest directory
theQmrDir=`dirname ${0}`
theQmrDir=`cd $theQmrDir; pwd`
theQmtDir=`cd $theQmtDir/../../CoolTest/qmtest; pwd`

# Go to the cmt directory and setup cmt
pushd ../../config/cmt > /dev/null
source CMT_env.sh > /dev/null
source setup.sh
popd >& /dev/null

# Go to the qmtest directory and produce the test summaries
cd $theQmtDir
for thePlatform in ${thePlatforms}; do
  if [ "${thePlatform}" != "" ]; then
    theXml=${theQmrDir}/${thePlatform}.xml
    theTim=${theQmrDir}/${thePlatform}.timing
    echo "Produce qmtest timing report from ${theXml}"
    if [ -f ${theXml} ]; then
      \rm -rf ${theTim}
      python=${theQmrDir}/performanceReport.py ${theXml} > ${theTim}
    else
      echo "ERROR! File not found: ${theXml}"
    fi
    echo "Done!"
  fi
done

